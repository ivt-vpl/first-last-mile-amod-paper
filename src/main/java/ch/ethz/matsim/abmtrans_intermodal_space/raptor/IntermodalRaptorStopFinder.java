package ch.ethz.matsim.abmtrans_intermodal_space.raptor;

import java.util.Collections;
import java.util.List;

import org.matsim.api.core.v01.population.Person;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Inject;

import ch.sbb.matsim.routing.pt.raptor.DefaultRaptorStopFinder;
import ch.sbb.matsim.routing.pt.raptor.InitialStop;
import ch.sbb.matsim.routing.pt.raptor.RaptorParameters;
import ch.sbb.matsim.routing.pt.raptor.RaptorStopFinder;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorData;

public class IntermodalRaptorStopFinder implements RaptorStopFinder {
	private final RaptorStopFinder delegate;

	@Inject
	public IntermodalRaptorStopFinder(DefaultRaptorStopFinder delegate) {
		this.delegate = delegate;
	}

	@Override
	public List<InitialStop> findStops(Facility facility, Person person, double departureTime,
			RaptorParameters parameters, SwissRailRaptorData data, Direction type) {
		if (facility instanceof TransitStopFacility) {
			return Collections.singletonList(new InitialStop((TransitStopFacility) facility, 0.0, 0.0, 0.0, "walk"));
		} else {
			return delegate.findStops(facility, person, departureTime, parameters, data, type);
		}
	}
}
