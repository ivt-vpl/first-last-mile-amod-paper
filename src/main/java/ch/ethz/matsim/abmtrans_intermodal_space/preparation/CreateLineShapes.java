package ch.ethz.matsim.abmtrans_intermodal_space.preparation;

import java.util.LinkedList;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.geotools.MGC;
import org.matsim.core.utils.gis.PolylineFeatureFactory;
import org.matsim.core.utils.gis.ShapeFileWriter;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitScheduleReader;
import org.opengis.feature.simple.SimpleFeature;

public class CreateLineShapes {
	private static class Line {
		final LineString geometry;
		final String name;

		public Line(LineString geometry, String name) {
			this.geometry = geometry;
			this.name = name;
		}
	}

	static public void main(String[] args) {
		String schedulePath = args[0];
		String outputPath = args[1];

		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);
		new TransitScheduleReader(scenario).readFile(schedulePath);

		GeometryFactory factory = new GeometryFactory();
		List<Line> lines = new LinkedList<>();

		for (TransitLine line : scenario.getTransitSchedule().getTransitLines().values()) {
			for (TransitRoute route : line.getRoutes().values()) {
				if (route.getTransportMode().equals("rail") && line.getId().toString().startsWith("SBB_S")) {
					List<Coordinate> coordinates = new LinkedList<>();

					for (TransitRouteStop stop : route.getStops()) {
						Coord stopCoord = stop.getStopFacility().getCoord();
						coordinates.add(new Coordinate(stopCoord.getX(), stopCoord.getY()));
					}

					if (coordinates.size() > 1) {
						LineString geometry = factory
								.createLineString(coordinates.toArray(new Coordinate[coordinates.size()]));
						lines.add(new Line(geometry, line.getId().toString()));
					}
				}
			}
		}

		List<SimpleFeature> features = new LinkedList<>();

		PolylineFeatureFactory featureFactory = new PolylineFeatureFactory.Builder() //
				.setCrs(MGC.getCRS("EPSG:2056")) //
				.setName("lines") //
				.addAttribute("name", String.class) //
				.create();

		for (Line line : lines) {
			features.add(featureFactory.createPolyline(line.geometry, new String[] { line.name }, null));
		}

		ShapeFileWriter.writeGeometries(features, outputPath);
	}
}
