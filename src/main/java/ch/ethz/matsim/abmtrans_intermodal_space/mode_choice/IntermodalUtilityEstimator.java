package ch.ethz.matsim.abmtrans_intermodal_space.mode_choice;

import java.util.LinkedList;
import java.util.List;

import org.eqasim.automated_vehicles.mode_choice.mode_parameters.AvModeParameters;
import org.eqasim.automated_vehicles.mode_choice.utilities.estimators.AvUtilityEstimator;
import org.eqasim.core.simulation.mode_choice.parameters.ModeParameters;
import org.eqasim.core.simulation.mode_choice.utilities.UtilityEstimator;
import org.eqasim.core.simulation.mode_choice.utilities.estimators.PtUtilityEstimator;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;

import com.google.inject.Inject;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;

public class IntermodalUtilityEstimator implements UtilityEstimator {
	private final AvUtilityEstimator avEstimator;
	private final PtUtilityEstimator ptEstimator;
	private final AvModeParameters avParameters;
	private final ModeParameters modeParameters;

	@Inject
	public IntermodalUtilityEstimator(AvUtilityEstimator avEstimator, PtUtilityEstimator ptEstimator,
			AvModeParameters avParameters, ModeParameters modeParameters) {
		this.avEstimator = avEstimator;
		this.ptEstimator = ptEstimator;
		this.avParameters = avParameters;
		this.modeParameters = modeParameters;
	}

	@Override
	public double estimateUtility(Person person, DiscreteModeChoiceTrip trip, List<? extends PlanElement> elements) {
		List<String> modes = new LinkedList<>();

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;
				modes.add(leg.getMode());
			}
		}

		int firstAvIndex = modes.indexOf("av");
		int lastAvIndex = modes.lastIndexOf("av");
		int firstPtIndex = modes.indexOf("access_walk");
		int lastPtIndex = modes.lastIndexOf("egress_walk");

		double utility = 0.0;

		if (firstAvIndex < firstPtIndex && firstAvIndex != -1 && firstPtIndex != -1) {
			List<? extends PlanElement> accessElements = elements.subList(0, 2 * firstPtIndex - 1);
			try {
				utility += avEstimator.estimateUtility(person, trip, accessElements);
			} catch (IllegalStateException e) {
				for (PlanElement element : accessElements) {
					System.out.println(element.toString());
				}
				throw new RuntimeException(e);
			}
			utility -= avParameters.alpha_u;
			utility += modeParameters.pt.betaLineSwitch_u;
		}

		if (lastAvIndex > lastPtIndex && lastAvIndex != -1 && lastPtIndex != -1) {
			List<? extends PlanElement> egressElements = elements.subList(2 * lastPtIndex + 2, elements.size());
			utility += avEstimator.estimateUtility(person, trip, egressElements);
			utility -= avParameters.alpha_u;
			utility += modeParameters.pt.betaLineSwitch_u;
		}

		if (firstPtIndex == -1 && firstAvIndex != -1) {
			throw new IllegalStateException("This should not happen! We have an av + access/egress walk here.");
		}

		int ptStartIndex = 0;
		int ptEndIndex = elements.size();

		if (firstPtIndex != -1) {
			ptStartIndex = 2 * firstPtIndex;
			ptEndIndex = 2 * lastPtIndex + 1;
		}

		List<? extends PlanElement> transitElements = elements.subList(ptStartIndex, ptEndIndex);
		utility += ptEstimator.estimateUtility(person, trip, transitElements);

		return utility;
	}
}
