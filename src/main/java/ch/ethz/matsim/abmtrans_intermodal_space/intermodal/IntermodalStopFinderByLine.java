package ch.ethz.matsim.abmtrans_intermodal_space.intermodal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

public class IntermodalStopFinderByLine implements IntermodalStopFinder {
	private static final Logger logger = Logger.getLogger(IntermodalStopFinderByLine.class);

	private final Collection<QuadTree<TransitStopFacility>> indices = new LinkedList<>();
	private final double maximumDistance_m;

	public IntermodalStopFinderByLine(Network network, TransitSchedule schedule, double maximumDistance_m) {
		this.maximumDistance_m = maximumDistance_m;
		double[] dimensions = NetworkUtils.getBoundingBox(network.getNodes().values());

		for (TransitLine transitLine : schedule.getTransitLines().values()) {
			QuadTree<TransitStopFacility> lineIndex = new QuadTree<>(dimensions[0], dimensions[1], dimensions[2],
					dimensions[3]);

			for (TransitRoute transitRoute : transitLine.getRoutes().values()) {
				if (transitRoute.getTransportMode().equals("rail")) {
					for (TransitRouteStop stop : transitRoute.getStops()) {
						TransitStopFacility stopFacility = stop.getStopFacility();
						lineIndex.put(stopFacility.getCoord().getX(), stopFacility.getCoord().getY(), stopFacility);
					}
				}
			}

			if (lineIndex.size() > 0) {
				logger.info(
						String.format("Found %d stops for line %s", lineIndex.size(), transitLine.getId().toString()));
				indices.add(lineIndex);
			}
		}
	}

	public Collection<TransitStopFacility> findStops(Coord location) {
		Collection<TransitStopFacility> result = new ArrayList<>(indices.size());

		for (QuadTree<TransitStopFacility> index : indices) {
			TransitStopFacility closestCandidate = index.getClosest(location.getX(), location.getY());
			double distance_m = CoordUtils.calcEuclideanDistance(location, closestCandidate.getCoord());

			if (distance_m <= maximumDistance_m) {
				result.add(index.getClosest(location.getX(), location.getY()));
			}
		}

		return result;
	}
}
