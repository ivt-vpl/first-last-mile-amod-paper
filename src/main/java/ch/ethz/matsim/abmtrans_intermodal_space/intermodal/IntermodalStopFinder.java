package ch.ethz.matsim.abmtrans_intermodal_space.intermodal;

import java.util.Collection;

import org.matsim.api.core.v01.Coord;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

public interface IntermodalStopFinder {
	Collection<TransitStopFacility> findStops(Coord location);
}
