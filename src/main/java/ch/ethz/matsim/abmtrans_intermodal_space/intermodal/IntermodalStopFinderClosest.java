package ch.ethz.matsim.abmtrans_intermodal_space.intermodal;

import java.util.Collection;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class IntermodalStopFinderClosest implements IntermodalStopFinder {
	private static final Logger logger = Logger.getLogger(IntermodalStopFinderClosest.class);

	private final QuadTree<TransitStopFacility> index;

	@Inject
	public IntermodalStopFinderClosest(Network network, TransitSchedule schedule) {
		double[] dimensions = NetworkUtils.getBoundingBox(network.getNodes().values());
		index = new QuadTree<>(dimensions[0], dimensions[1], dimensions[2], dimensions[3]);

		for (TransitLine transitLine : schedule.getTransitLines().values()) {
			for (TransitRoute transitRoute : transitLine.getRoutes().values()) {
				if (transitRoute.getTransportMode().equals("rail")) {
					for (TransitRouteStop stop : transitRoute.getStops()) {
						TransitStopFacility stopFacility = stop.getStopFacility();
						index.put(stopFacility.getCoord().getX(), stopFacility.getCoord().getY(), stopFacility);
					}
				}
			}
		}
	}

	public Collection<TransitStopFacility> findStops(Coord location) {
		return Collections.singleton(index.getClosest(location.getX(), location.getY()));
	}
}
