package ch.ethz.matsim.abmtrans_intermodal_space.intermodal;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

public class IntermodalRoutingModule implements RoutingModule {
	private final RoutingModule avRoutingModule;
	private final RoutingModule ptRoutingModule;
	private final PopulationFactory populationFactory;
	private final IntermodalStopFinder stopFinder;

	public IntermodalRoutingModule(RoutingModule avRoutingModule, RoutingModule ptRoutingModule,
			PopulationFactory populationFactory, IntermodalStopFinder stopFinder) {
		this.avRoutingModule = avRoutingModule;
		this.ptRoutingModule = ptRoutingModule;
		this.populationFactory = populationFactory;
		this.stopFinder = stopFinder;
	}

	private double getTravelTime(List<? extends PlanElement> elements) {
		double travelTime = 0.0;

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;
				travelTime += leg.getTravelTime();
			}
		}

		return travelTime;
	}

	private Activity createInteractionActivity(TransitStopFacility facility) {
		Activity activity = populationFactory.createActivityFromLinkId("av interaction", facility.getLinkId());
		activity.setMaximumDuration(0.0);
		return activity;
	}

	private boolean isOnlyWalk(List<? extends PlanElement> elements) {
		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;

				if (leg.getMode().equals("pt") || leg.getMode().equals("av")) {
					return false;
				}
			}
		}

		return true;
	}

	@Override
	public List<? extends PlanElement> calcRoute(Facility fromFacility, Facility toFacility, double departureTime,
			Person person) {
		// Get direct travel time as upper bound
		List<? extends PlanElement> fastestRoute = ptRoutingModule.calcRoute(fromFacility, toFacility, departureTime,
				person);

		if (!isOnlyWalk(fastestRoute)) {
			double fastestTravelTime = getTravelTime(fastestRoute);

			Collection<TransitStopFacility> accessFacilityCandidates = stopFinder.findStops(fromFacility.getCoord());
			Collection<TransitStopFacility> egressFacilityCandidates = stopFinder.findStops(toFacility.getCoord());

			// Assess each access facility candidate with walk egress

			for (TransitStopFacility accessFacility : accessFacilityCandidates) {
				List<? extends PlanElement> accessRoute = avRoutingModule.calcRoute(fromFacility, accessFacility,
						departureTime, person);
				double accessTravelTime = getTravelTime(accessRoute);

				if (accessTravelTime < fastestTravelTime && !isOnlyWalk(accessRoute)) {
					List<? extends PlanElement> transitRoute = ptRoutingModule.calcRoute(accessFacility, toFacility,
							departureTime + accessTravelTime, person);
					double transitTravelTime = getTravelTime(transitRoute);

					if (accessTravelTime + transitTravelTime < fastestTravelTime && !isOnlyWalk(transitRoute)) {
						List<PlanElement> compositeRoute = new LinkedList<>();

						compositeRoute.addAll(accessRoute);
						compositeRoute.add(createInteractionActivity(accessFacility));
						compositeRoute.addAll(transitRoute);

						fastestRoute = compositeRoute;
						fastestTravelTime = accessTravelTime + transitTravelTime;
					}
				}
			}

			// Assess each egress facility candidate with walk access

			for (TransitStopFacility egressFacility : accessFacilityCandidates) {
				List<? extends PlanElement> transitRoute = ptRoutingModule.calcRoute(fromFacility, egressFacility,
						departureTime, person);
				double transitTravelTime = getTravelTime(transitRoute);

				if (transitTravelTime < fastestTravelTime && !isOnlyWalk(transitRoute)) {
					List<? extends PlanElement> egressRoute = avRoutingModule.calcRoute(egressFacility, toFacility,
							departureTime + transitTravelTime, person);
					double egressTravelTime = getTravelTime(egressRoute);

					if (transitTravelTime + egressTravelTime < fastestTravelTime && !isOnlyWalk(egressRoute)) {
						List<PlanElement> compositeRoute = new LinkedList<>();

						compositeRoute.addAll(transitRoute);
						compositeRoute.add(createInteractionActivity(egressFacility));
						compositeRoute.addAll(egressRoute);

						fastestRoute = compositeRoute;
						fastestTravelTime = transitTravelTime + egressTravelTime;
					}
				}
			}

			// Assess each combination of access and egress facility candidate

			for (TransitStopFacility accessFacility : accessFacilityCandidates) {
				List<? extends PlanElement> accessRoute = avRoutingModule.calcRoute(fromFacility, accessFacility,
						departureTime, person);
				double accessTravelTime = getTravelTime(accessRoute);

				if (accessTravelTime < fastestTravelTime && !isOnlyWalk(accessRoute)) {
					for (TransitStopFacility egressFacility : egressFacilityCandidates) {
						List<? extends PlanElement> transitRoute = ptRoutingModule.calcRoute(accessFacility,
								egressFacility, departureTime + accessTravelTime, person);
						double transitTravelTime = getTravelTime(transitRoute);

						if (accessTravelTime + transitTravelTime < fastestTravelTime && !isOnlyWalk(transitRoute)) {
							List<? extends PlanElement> egressRoute = avRoutingModule.calcRoute(egressFacility,
									toFacility, departureTime + accessTravelTime + transitTravelTime, person);
							double egressTravelTime = getTravelTime(egressRoute);

							if (accessTravelTime + transitTravelTime + egressTravelTime < fastestTravelTime
									&& !isOnlyWalk(egressRoute)) {
								List<PlanElement> compositeRoute = new LinkedList<>();

								compositeRoute.addAll(accessRoute);
								compositeRoute.add(createInteractionActivity(accessFacility));
								compositeRoute.addAll(transitRoute);
								compositeRoute.add(createInteractionActivity(egressFacility));
								compositeRoute.addAll(egressRoute);

								fastestRoute = compositeRoute;
								fastestTravelTime = accessTravelTime + transitTravelTime + egressTravelTime;
							}
						}
					}
				}
			}
		}

		return fastestRoute;
	}

	@Override
	public StageActivityTypes getStageActivityTypes() {
		return new StageActivityTypesImpl("av interaction");
	}
}
