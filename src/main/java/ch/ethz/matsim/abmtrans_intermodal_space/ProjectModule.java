package ch.ethz.matsim.abmtrans_intermodal_space;

import org.eqasim.automated_vehicles.mode_choice.utilities.estimators.AvUtilityEstimator;
import org.eqasim.core.simulation.mode_choice.AbstractEqasimExtension;
import org.eqasim.core.simulation.mode_choice.utilities.estimators.PtUtilityEstimator;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.router.RoutingModule;
import org.matsim.pt.transitSchedule.api.TransitSchedule;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.abmtrans_intermodal_space.intermodal.IntermodalRoutingModule;
import ch.ethz.matsim.abmtrans_intermodal_space.intermodal.IntermodalStopFinder;
import ch.ethz.matsim.abmtrans_intermodal_space.intermodal.IntermodalStopFinderByLine;
import ch.ethz.matsim.abmtrans_intermodal_space.intermodal.IntermodalStopFinderClosest;
import ch.ethz.matsim.abmtrans_intermodal_space.intermodal.IntermodalTripConstraint;
import ch.ethz.matsim.abmtrans_intermodal_space.mode_choice.IntermodalUtilityEstimator;
import ch.ethz.matsim.abmtrans_intermodal_space.raptor.IntermodalRaptorStopFinder;
import ch.sbb.matsim.routing.pt.raptor.DefaultRaptorStopFinder;
import ch.sbb.matsim.routing.pt.raptor.RaptorStopFinder;

public class ProjectModule extends AbstractEqasimExtension {
	@Override
	protected void installEqasimExtension() {
		bindModeAvailability(ProjectModeAvailability.NAME).to(ProjectModeAvailability.class);

		bind(PtUtilityEstimator.class);
		bind(AvUtilityEstimator.class);

		bindUtilityEstimator("IntermodalUtilityEstimator").to(IntermodalUtilityEstimator.class);
		bindTripConstraintFactory("IntermodalTripConstraint").to(IntermodalTripConstraint.Factory.class);

		addRoutingModuleBinding("intermodal").to(IntermodalRoutingModule.class);

		bind(RaptorStopFinder.class).to(IntermodalRaptorStopFinder.class);
		bind(DefaultRaptorStopFinder.class);

		bind(IntermodalStopFinder.class).to(IntermodalStopFinderClosest.class);
	}

	@Provides
	@Singleton
	public IntermodalStopFinderByLine provideIntermodalStopFinderByLine(Network network, TransitSchedule schedule) {
		double maximumDistance_m = 5000.0;
		return new IntermodalStopFinderByLine(network, schedule, maximumDistance_m);
	}

	@Provides
	public IntermodalRoutingModule provideIntermodalRoutingModule(@Named("av") RoutingModule avRoutingModule,
			@Named("pt") RoutingModule ptRoutingModule, Population population, IntermodalStopFinder stopFinder) {
		return new IntermodalRoutingModule(avRoutingModule, ptRoutingModule, population.getFactory(), stopFinder);
	}
}
